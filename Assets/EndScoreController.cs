﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndScoreController : MonoBehaviour {
    private Text ThisText;
    public string playerPref;
	// Use this for initialization
	void Start () {
        ThisText = GetComponent<Text>();
        ThisText.text = PlayerPrefs.GetFloat(playerPref, 0).ToString();
    }
	
	
}
