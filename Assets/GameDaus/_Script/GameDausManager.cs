﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameDausManager : MonoBehaviour {

    private ScoreManager scriptScoreManager;
    public float Score = 0;
    public Text textScore;
    public GameObject UI;
	// Use this for initialization
	void Start () {
        Score = 0;
        PlayerPrefs.SetFloat("score", 0);
        //scriptScoreManager = GameObject.FindGameObjectWithTag("GlobalManager").GetComponent<ScoreManager>();
	}

    void OnEnable()
    {
        if (UI != null)
            UI.SetActive(true);
    }
    void OnDisable()
    {
        if(UI != null)
            UI.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
	    
	}

    public void AddScoreInGameDaus(float _val)
    {
        Score += _val;

        if (textScore != null)
            textScore.text = Score.ToString();

        PlayerPrefs.SetFloat("score", Score);
    }
}
