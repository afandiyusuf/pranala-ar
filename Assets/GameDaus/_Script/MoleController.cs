﻿using UnityEngine;
using System.Collections;

public class MoleController : MonoBehaviour {

    bool isReady;
    public bool swipped;
    private MoleMaster moleMaster;

    public float appearTime;
    public float dissapearTime;
    public bool ReadyToSwipe;

    private Vector3 initPosition;
    private Vector3 initScale;

    void Start()
    {
        initPosition = gameObject.transform.localPosition;
        initScale = gameObject.transform.localScale;
        moleMaster = GameObject.FindGameObjectWithTag("GameManagerDaus").GetComponent<MoleMaster>();
        DissapearAndStandby();
    }

    public void InitThisMole()
    {
        swipped = false;
        ReadyToSwipe = false;
        gameObject.transform.localPosition = initPosition;
        gameObject.transform.localScale = initScale;
        DissapearAndStandby();
    }

    public void GoUp()
    {
        if (swipped)
            return;

        iTween.MoveTo(gameObject, iTween.Hash("isLocal", true, "z", moleMaster.upPosition));
        Invoke("AppearsAndStandby", 0.5f);
    }

    public void AppearsAndStandby()
    {
        if (swipped)
            return;
        ReadyToSwipe = true;
        appearTime = Random.Range(moleMaster.appearMin, moleMaster.appearMax);

        Invoke("GoDown", appearTime);
    }

    public void GoDown()
    {
        if (swipped)
            return;
        ReadyToSwipe = false;
        iTween.MoveTo(gameObject, iTween.Hash("isLocal", true, "z", moleMaster.downPosition));
        Invoke("DissapearAndStandby", 0.5f);
    }

    public void DissapearAndStandby()
    {
        if (swipped)
            return;
        dissapearTime = Random.Range(moleMaster.DelayMin, moleMaster.DelayMax);
        Invoke("GoUp", dissapearTime);
    }
}
