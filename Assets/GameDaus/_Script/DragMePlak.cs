﻿using UnityEngine;
using System.Collections;

public class DragMePlak : MonoBehaviour {
    private Vector3 v3Offset;
    private Plane plane;
    private MoleController moleController;
    private GameDausManager scriptGameDausManager;
    private AudioSource thisAudioSource;
    
    void Start()
    {
        thisAudioSource = GameObject.FindGameObjectWithTag("GetCoinSound").GetComponent<AudioSource>();
        moleController = this.GetComponent<MoleController>();
        scriptGameDausManager = GameObject.FindGameObjectWithTag("GameManagerDaus").GetComponent<GameDausManager>();
    }
    void OnMouseDown()
    {
        if (moleController.ReadyToSwipe)
        {
            //tranformDaus.LookAt(this.transform);
            iTween.Stop(gameObject);
            plane.SetNormalAndPosition(Camera.main.transform.forward, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float dist;
            plane.Raycast(ray, out dist);
            v3Offset = transform.position - ray.GetPoint(dist);
        }
    }

    void OnMouseDrag()
    {
        if (moleController.ReadyToSwipe)
        {
            moleController.swipped = true;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float dist;
            plane.Raycast(ray, out dist);
            Vector3 v3Pos = ray.GetPoint(dist);
            transform.position = v3Pos + v3Offset;
        }
    }

    void OnMouseUp()
    {
        if (moleController.ReadyToSwipe)
        {
            TweenKeBawah();
            scriptGameDausManager.AddScoreInGameDaus(50);
            thisAudioSource.Play();
            //iTween.MoveTo(this.gameObject, Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 2)), 2); //ke atas
            //iTween.ScaleTo(this.gameObject, iTween.Hash("x", 0.03f, "y", 0.03f, "z", 0.03f, "time", 2));

            //Invoke("InitThisMole", 0.5f);
        }
    }

    void TweenKeBawah()
    {
        iTween.MoveTo(this.gameObject, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, 0, 0)), 1); //ke atas
        //iTween.ScaleTo(this.gameObject, iTween.Hash("x", -0.03f, "y", -0.03f, "z", -0.03f, "time", 1));
        Invoke("InitThisMole", 0.8f);
    }

    void InitThisMole()
    {
        iTween.Stop(gameObject);
        moleController.InitThisMole();
    }
}
