﻿using UnityEngine;
using System.Collections;

public class MoleMaster : MonoBehaviour {
    [Header("Nilai Y Mole")]
    public float upPosition;
    public float downPosition;

    [Header("Timer")]
    public float appearMin;
    public float appearMax;
    public float DelayMin;
    public float DelayMax;
}
