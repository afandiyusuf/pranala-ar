﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ExistingDBScript : MonoBehaviour {

	public Text DebugText;

	// Use this for initialization
	void Start () {
		var ds = new DataService ("existing.db");
        ds.InitDb();
        ds.CreateInitDb();
        var people = ds.GetAllMission ();
 
        ToConsole(people);
        
        /*
		people = ds.GetPersonsNamedRoberto ();
		ToConsole("Searching for Roberto ...");
		ToConsole (people);

		ds.CreatePerson ();
		ToConsole("New person has been created");
		var p = ds.GetJohnny ();
		ToConsole(p.ToString());
        */

    }
	
	

	private void ToConsole(string msg){
		DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}

    private void ToConsole(IEnumerable<MissionData> misDatas)
    {
        foreach (var misData in misDatas)
        {
            ToConsole(misData.ToString());
        }
    }

}
