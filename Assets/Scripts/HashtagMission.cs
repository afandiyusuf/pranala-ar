﻿using UnityEngine;
using System.Collections;

public class HashtagMission : MonoBehaviour {

    public static string JEMPUT_PROGRES = "jemput progres";
    public static string JARAK_SEKALI_MAIN = "jarak ingame";
    public static string ISI_BENSIN_SEKALI_MAIN = "bensin sekali main";
    public static string SETORAN_SEKALI_MAIN = "Setoran Sekali main";
    public static string SETORAN_PROGRESS = "Setoran Progress";
    public static string JARAK_PROGRESS = "Jarak Progress";
    public static string ISI_BENSIN_PROGRESS = "Isi Bensin Progress";
    public static string HINDARI_OBSTACLE_SEKALI = "Hindari Obstacle Sekali";
    public static string HINDARI_OBSTACLE_PROGRESS = "Hindari Obstacle Progress";

}
