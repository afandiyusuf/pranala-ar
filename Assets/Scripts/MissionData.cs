﻿using SQLite4Unity3d;

public class MissionData  {

	[PrimaryKey, AutoIncrement]
	public int id { get; set; }
    public string user_name { get;set; }
	public string mission_name { get; set; }
    public string mission_description { get; set; }
    public float total_tiers { get; set; }
	public float tiers_complete { get; set; }
    public string tiers_vals { get; set; }
    public bool is_active { get; set; }
    public float tiers_progress { get; set; }
    public string mission_type { get; set; }
    public bool now_complete { get; set; }
    public string last_update { get; set; }
    public string id_user { get; set; }

    public override string ToString ()
	{
		return string.Format ("[Person: id={0},  mission_name={1}, mission_description={2}, total_tiers={3}, tiers_complete={4}, tiers_vals{5}, is_active={6}, mission_type={7}, last_update={8},username={9}]", id, mission_name,mission_description, total_tiers, tiers_complete,tiers_vals, is_active,mission_type,last_update,user_name);
	}

}
