﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    public GameObject AllUi;
    public GameObject UI1;
    public GameObject UI2;
    public GameObject UI3;
    public GameObject ThanksUI;
    public CreateDBSampoerna dsSampoerna;
    private int indexSelected;

    public Text t1;
    public Text t2;
    public Text t3;

    void Start()
    {
        dsSampoerna = GameObject.FindGameObjectWithTag("DataService").GetComponent<CreateDBSampoerna>();

        Invoke("UpdateText", 1);
       
    }
    public void UpdateText()
    {
        t1.text = "Pemilih no 1: "+dsSampoerna.GetTotalVote(1).ToString();
        t2.text = "Pemilih no 2: " + dsSampoerna.GetTotalVote(2).ToString();
        t3.text = "Pemilih no 3: " + dsSampoerna.GetTotalVote(3).ToString();
    }
    public void EnableUI(int index)
    {
        UI1.SetActive(false);
        UI2.SetActive(false);
        UI3.SetActive(false);

        switch (index)
        {
            case 1:
                UI1.SetActive(true);
                break;
            case 2:
                UI2.SetActive(true);
                break;
            case 3:
                UI3.SetActive(true);
                break;
        }
        indexSelected = index;
        AllUi.SetActive(true);
    }

    public void DisableUI()
    {
        AllUi.SetActive(false);
    }

    public void BackToSelect()
    {
        GameObject[] selectableObjs = GameObject.FindGameObjectsWithTag("SelectableObjects");
        for (int i = 0; i < selectableObjs.Length; i++)
        {
            DragToPick tempObj = selectableObjs[i].GetComponentInChildren<DragToPick>();
            tempObj.enabled = true;
            iTween.MoveTo(tempObj.redererObj, tempObj.gameObject.transform.position, 1);
            DisableUI();
        }
    }
    public void StartVoting()
    {
        dsSampoerna.StartVoting(indexSelected);
        ThanksUI.SetActive(true);
        Invoke("RestartScreen",3);
  
    }

    public void RestartScreen()
    {
        Application.LoadLevel("VotingScreen");
    }
}
