﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    private AudioSource audioSource;
    public AudioClip soundBG;
    public AudioClip soundInGame;
    public AudioClip soundTap;

    public AudioClip thisBGM;
    // Use this for initialization
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        if (thisBGM != null)
        {
            audioSource.clip = thisBGM;
            audioSource.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlaySoundMenu()
    {
        audioSource.clip = soundBG;
        audioSource.Play();
    }

    public void PlaySoundInGame()
    {
        audioSource.clip = soundInGame;
        audioSource.Play();
    }

    public void PlaySoundTap()
    {
        audioSource.PlayOneShot(soundTap);
    }
}
