﻿using UnityEngine;
using System.Collections;

public class MasterController : MonoBehaviour {

    public GameObject targetPosition;
    public int CurrentSelected;

    public UIController uiController;

    public void SentToTarget(GameObject targetAnimation,int index)
    {
        CurrentSelected = index;

        iTween.MoveTo(targetAnimation, targetPosition.transform.position,1);
        uiController.EnableUI(index);
        DragToPick[] allDrag = gameObject.GetComponentsInChildren<DragToPick>();
        for(int i=0;i<allDrag.Length;i++)
        {
            allDrag[i].enabled = false;
        }
    }

    public void BackSelect()
    {

    }

    
}
