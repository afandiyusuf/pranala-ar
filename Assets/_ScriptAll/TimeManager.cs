﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {

    private float theTimer = 0;
    private float floatCurrentTimer = 0;
    public float theStartTime = 60;

    public bool gameIsPlaying = false;

    private NavigationManager scriptNavigationManager;

    public Image barTimer;

    // Use this for initialization
    void Start()
    {
        scriptNavigationManager = this.GetComponent<NavigationManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameIsPlaying)
        {
            barTimer.fillAmount -= theTimer;
            floatCurrentTimer = barTimer.fillAmount;

            if (floatCurrentTimer <= 0)
            {
                gameIsPlaying = false;
                scriptNavigationManager.EndGame();
            }
        }
    }

    public void CheckGameIsPlaying(bool isPlaying)
    {
        if (isPlaying)
        {
            gameIsPlaying = true;
            theTimer = 1 / (theStartTime * 60);
            barTimer.fillAmount = 1;
        }
        else
        {
            gameIsPlaying = false;
        }
    }
}
