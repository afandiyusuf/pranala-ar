﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vuforia;

public class CameraManager : MonoBehaviour {

    private int totNoImage;

    private bool preGame;
    private bool startGame;

    public GameObject GameDaus;
    public GameObject GameDeny;
    public GameObject GameDian;
    public GameObject GameFirah;

    public GameObject textWarning;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        string nameTrackable = "";

        // Get the StateManager
        StateManager sm = TrackerManager.Instance.GetStateManager();

        // Query the StateManager to retrieve the list of
        // currently 'active' trackables 
        //(i.e. the ones currently being tracked by Vuforia)
        IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours();

        // Iterate through the list of active trackables
        int numImageTargets = 0;
        foreach (TrackableBehaviour tb in activeTrackables)
        {
            if (tb is ImageTargetBehaviour)
            {
                numImageTargets++;
                nameTrackable = tb.TrackableName;
            }
        }

        totNoImage = numImageTargets;

        if (totNoImage == 0)
        {
            textWarning.SetActive(true);
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
        }
        else
        {
            textWarning.SetActive(false);
        }

        if (!preGame && totNoImage == 1)
        {
            preGame = true;
            startGame = true; 
        }

        if (startGame)
        {
            switch (nameTrackable)
            {
                case "01d_Daus_inmocARd":
                    GameDaus.SetActive(true);
                    break;
                case "01d_Denny_Aryadi_inmocARd":
                    GameDaus.SetActive(true);
                    break;
                case "01d_Dian_inmocARd":
                    GameDaus.SetActive(true);
                    break;
                case "01d_Firah_inmocARd":
                    GameDaus.SetActive(true);
                    break;
                
            }
        }
	}
}
