﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SwipeControl : MonoBehaviour
{

    public enum SwipeDirection
    {
        Up,
        Down,
        Right,
        Left
    }

    public SwipeDirection swipe;
    private bool swiping = false;
    private bool eventSent = false;
    private Vector2 lastPosition;

    void Update()
    {
        
        if (Input.touchCount == 0)
            return;

        if (Input.GetTouch(0).deltaPosition.sqrMagnitude != 0)
        {
            if (swiping == false)
            {
                swiping = true;
                lastPosition = Input.GetTouch(0).position;
                return;
            }
            else
            {
                if (!eventSent)
                {
                    if (swipe != null)
                    {
                        Vector2 direction = Input.GetTouch(0).position - lastPosition;

                        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                        {
                            if (direction.x > 0)
                            {
                                Debug.Log("right");
                                swipe = SwipeDirection.Right;
                            }
                            else
                            {
                                Debug.Log("left");
                                swipe = SwipeDirection.Left;
                            }
                        }
                        else
                        {
                            if (direction.y > 0)
                            {
                                Debug.Log("top");
                                swipe = SwipeDirection.Up;
                            }
                            else
                            {
                                Debug.Log("bot");
                                swipe = SwipeDirection.Down;
                            }
                        }

                        eventSent = true;
                    }
                }
            }
        }
        else
        {
            swiping = false;
            eventSent = false;
        }
    }
}