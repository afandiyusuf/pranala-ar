﻿using UnityEngine;
using System.Collections;

public class DragMeBird : MonoBehaviour
{
    private float dist;
    private Vector3 v3Offset;
    private Plane plane;
    //public GameManager GM;
    //public TextMesh DebugText;
    public float ManaConsumtion = 10f;
    public bool isSanny = false;
    public GameObject DisapearExplosion;
    //SoundManagerPaddlepopAR Sounds;
    public GameManagerFirah GMFirah;

    AudioSource thisAudioSource;
    public Transform thisparent;
    public GameObject SplashParticles;
    void Update()
    {
        //thisparent = this.transform.parent;
    }
    void Start()
    {
        thisAudioSource = GameObject.FindGameObjectWithTag("GetCoinSound").GetComponent<AudioSource>();
        Destroy(gameObject, 20);
        GMFirah = GameObject.FindGameObjectWithTag("GameManagerFirah").GetComponent<GameManagerFirah>();

        //Sounds = Camera.main.GetComponent<SoundManagerPaddlepopAR>();
        //Sounds.PlaySounds(Sounds.TangkapSound);
        //Sounds.playAdaCoin();
        // isSanny = gameObject.GetComponent<RandomMovementWithVel>().isSanny;

        //DebugText = GameObject.FindGameObjectWithTag(TagRegister.DEBUG_TEXT).GetComponent<TextMesh>();
        //GM = Camera.main.GetComponent<UsedGameManager>().usedGameManger;
    }
    void OnMouseDown()
    {

        if (isSanny)
        {
            thisAudioSource.Play();
            //Instantiate(DisapearExplosion, gameObject.transform.position, transform.rotation);
            //GM.StoreToBucket(ManaConsumtion, isSanny);
            Instantiate(SplashParticles,this.gameObject.transform.position,Quaternion.identity);
            Destroy(gameObject);
            GMFirah.AddScore(50);

           // Sounds.playGetCoin();
        }
        else
        {

            plane.SetNormalAndPosition(Camera.main.transform.forward, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float dist;
            plane.Raycast(ray, out dist);
            v3Offset = transform.position - ray.GetPoint(dist);
           // Sounds.PlaySounds(Sounds.TapSound);
        }
    }

    void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float dist;
        plane.Raycast(ray, out dist);
        Vector3 v3Pos = ray.GetPoint(dist);
        transform.position = v3Pos + v3Offset;

    }

    void OnMouseUp()
    {
        /*
        //DebugText.text = Camera.main.WorldToScreenPoint(transform.position).ToString();
        if (GM.isAtBucket(Camera.main.WorldToScreenPoint(transform.position)))
        {
            GM.StoreToBucket(ManaConsumtion, isSanny);

            Instantiate(DisapearExplosion, gameObject.transform.position, transform.rotation);

            SoundManagerPaddlepopAR Sounds = Camera.main.GetComponent<SoundManagerPaddlepopAR>();
            Sounds.playGetBBird();

            Destroy(gameObject);
        }
        else
        {

        }*/
    }


}