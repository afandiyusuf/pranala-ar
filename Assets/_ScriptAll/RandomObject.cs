﻿using UnityEngine;
using System.Collections;

public class RandomObject : MonoBehaviour {

    public Transform[] thisChildrens;

    void Start()
    {
       // thisChildrens = gameObject.GetComponentsInChildren<Transform>();
        int randomIndex = Mathf.FloorToInt(Random.Range(0, thisChildrens.Length));

        for(int i=0;i<thisChildrens.Length;i++)
        {
           // Debug.Log("i is " + i + " , random is " + randomIndex);

            if (randomIndex == i || thisChildrens[i] == this.gameObject)
                thisChildrens[i].gameObject.SetActive(true);
            else
                thisChildrens[i].gameObject.SetActive(false);
        }

    }
}
