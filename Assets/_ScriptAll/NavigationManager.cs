﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class NavigationManager : MonoBehaviour {
    public GameObject canvasTutorial;
    public GameObject canvasUiGame;
    public GameObject canvasEndSreen;
    public GameObject pictureTapAnywhere;
    public GameObject loadingBarInTutorial;
    public GameObject loadingBarInEndScreen;
    public GameObject btnPlayAgain;


    private TimeManager scriptTimeManager;
    private ScoreManager scriptScoreManager;
    private SoundManager scriptSoundManager;

    private int totalPlaying = 0;

	// Use this for initialization
	void Start () {
        scriptTimeManager = this.GetComponent<TimeManager>();
        scriptScoreManager = this.GetComponent<ScoreManager>();
        scriptSoundManager = this.GetComponent<SoundManager>();
	}
	
	// Update is called once per frame
	void Update () {
        
	}


    #region fungsi memulai game
    /// <summary>
    /// fungsi untuk memulai game
    /// </summary>
    public void StartGame()
    {
        

        scriptSoundManager.PlaySoundTap();
        scriptScoreManager.score = 0;
        scriptScoreManager.textScoreInGameScreen.text = scriptScoreManager.score.ToString();

        pictureTapAnywhere.SetActive(false);
        loadingBarInTutorial.SetActive(true);

        Invoke("HideMenuTutorialAndShowUiGame", 1f);
        
    }

    private void HideMenuTutorialAndShowUiGame()
    {
        scriptSoundManager.PlaySoundInGame();
        loadingBarInTutorial.SetActive(false);
        canvasTutorial.SetActive(false);
        canvasUiGame.SetActive(true);
        pictureTapAnywhere.SetActive(true);
        scriptTimeManager.CheckGameIsPlaying(true);
        scriptScoreManager.achievmentNewHighScore.SetActive(false);
    }
    #endregion

    #region fungsi restart game
    /// <summary>
    /// fungsi untuk restart game
    /// </summary>
    public void PlayGameAgain()
    {
        scriptSoundManager.PlaySoundTap();
        totalPlaying += 1;

        if (totalPlaying > 3)
        {
            Application.LoadLevel(0);
        }
        else
        {
            scriptScoreManager.score = 0;
            scriptScoreManager.textScoreInGameScreen.text = scriptScoreManager.score.ToString();

            btnPlayAgain.SetActive(false);
            loadingBarInEndScreen.SetActive(true);
            Invoke("HideMenuEndScreenAndShowUiGame", 1f);
        }
    }

    private void HideMenuEndScreenAndShowUiGame()
    {
        loadingBarInEndScreen.SetActive(false);
        canvasEndSreen.SetActive(false);
        canvasTutorial.SetActive(true);
        scriptScoreManager.achievmentNewHighScore.SetActive(false);
    }
    #endregion

    public void EndGame()
    {
        scriptSoundManager.PlaySoundMenu();
        scriptScoreManager.isGameOver = true;
        scriptScoreManager.rollCurrentScore = 0;
        scriptScoreManager.textScoreInEndScreen.text = scriptScoreManager.rollCurrentScore.ToString();
        scriptScoreManager.RollCurrentScore();

        scriptTimeManager.CheckGameIsPlaying(false);
        canvasUiGame.SetActive(false);
        canvasEndSreen.SetActive(true);
    }
}
