﻿using UnityEngine;
using System.Collections;

public class RandomMovement : MonoBehaviour
{


    private Vector3 target;
    private Vector3 arah;
    private Vector3 newArah;
    public Vector3 minArah;
    public Vector3 maxArah;
    public float speedNikung = 1f;
    public float speed = 0.005f;
    public float baseSpeedMultiplier = 1f;
    public float SpeeDMultiPlier = 1f;
    public float lifeTime = 60f;
    private float currTime = 0f;
    public float timerSpeedMultiplier = 5f;

    private float currTimeMultiplier;


    //	private Vector3 posBefore;
    public bool isSanny = false;
    //private GameManager GM;

    private float currentTime;
    // Use this for initialization
    void Start()
    {
        //transform.localRotation = Quaternion.Euler(new Vector3(0,transform.localRotation.y,transform.localRotation.z));

        //GM = GameObject.FindGameObjectWithTag(TagRegister.GAME_MANAGER).GetComponent<GameManager>();
        //minArah = GM.minArea;
       // maxArah = GM.maxArea;

        //	posBefore = new Vector3(1f,1f,1f);

        //target = randomNewPos();
        target = randomNewPos();
        arah = (target - transform.position).normalized;
    }

    Vector3 randomNewPos()
    {
        return new Vector3(Random.Range(minArah.x, maxArah.x), Random.Range(minArah.y, maxArah.y), Random.Range(minArah.z, maxArah.z));
    }
    void FixedUpdate()
    {

        currentTime += Time.deltaTime;
        if (currentTime > 1)
        {

            currentTime = 0;
        }
    }
    // Update is called once per frame
    void Update()
    {

        currTimeMultiplier += Time.deltaTime;
        //Speed increase after 5 sec
        if (currTimeMultiplier > timerSpeedMultiplier)
        {
            baseSpeedMultiplier += SpeeDMultiPlier;
            currTimeMultiplier = 0;
        }

        currTime += Time.deltaTime;

        if (((target - transform.localPosition).magnitude < -2) || ((target - transform.localPosition).magnitude > 2))
        {
            target = randomNewPos();
            newArah = (target - transform.localPosition).normalized;
        }

        //Debug.Log(arah);
        //Debug.Log(newArah);
        arah = Vector3.Lerp(arah, newArah, speedNikung * Time.deltaTime);
        transform.localPosition = (transform.localPosition + arah * speed * baseSpeedMultiplier);

        Quaternion rotation = Quaternion.LookRotation(newArah);
        //transform.rotation = Quaternion.Lerp(transform.localRotation,rotation,Time.deltaTime);	

        //Bird die if after lifetime sec still exist
        if (currTime > lifeTime)
        {
            Destroy(gameObject);
        }
        //bunuh burung yang gak gerak!!!
        if (arah == newArah && !isSanny)
        {
            //Destroy(gameObject);
        }

    }
}