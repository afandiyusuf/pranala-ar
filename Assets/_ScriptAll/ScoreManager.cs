﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public int score = 0;
    public int bestScore;
    public bool isRollCurrentScore = false;
    public int rollCurrentScore = 0;

    public Text textScoreInEndScreen;
    public Text textScoreInGameScreen;
    public Text textBestScore;
    public bool isGameOver = false;
    public GameObject achievmentNewHighScore;

    private NavigationManager scriptNavigationManager;

	// Use this for initialization
	void Start () {
        scriptNavigationManager = this.GetComponent<NavigationManager>();

        bestScore = PlayerPrefs.GetInt("BestScore");
	}
	
	// Update is called once per frame
	void Update () {
        if (isGameOver)
        {
            if (isRollCurrentScore && (rollCurrentScore <= score))
            {
                rollCurrentScore += 1;
                textScoreInEndScreen.text = rollCurrentScore.ToString();
            }
            else
            {
                isRollCurrentScore = false;
                StopRollScore();
            }
        }
	}

    public void AddScore()
    {
        score += 10;
        textScoreInGameScreen.text = score.ToString();
    }

    public void RollCurrentScore()
    {
        isRollCurrentScore = true;
    }

    public void StopRollScore()
    {
        isRollCurrentScore = false;
        textScoreInEndScreen.text = score.ToString();

        if (score > bestScore)
        {
            achievmentNewHighScore.SetActive(true);
            PlayerPrefs.SetInt("BestScore", score);
            textBestScore.text = score.ToString();
        }
        else
        {
            textBestScore.text = bestScore.ToString();
        }

        scriptNavigationManager.btnPlayAgain.SetActive(true);
        isGameOver = false;

        
    }
}
