﻿using UnityEngine;
using System.Collections;

public class DragToPick : MonoBehaviour
{

   // private TextMesh textMesh;
   // private Color startColor;
    private Vector3 deltaPosition;
    private int fingerIndex;
    private float positionAtCamera;
    private float initPosition;
    private Vector3 initObjPos;
    private float endPos;
    public int thisIndex;
    public float tolerance;
    CreateDBSampoerna dataService;
    public MasterController masterController;
    public GameObject redererObj;

    // Subscribe to events
    void OnEnable()
    {
        EasyTouch.On_Drag += On_Drag;
        EasyTouch.On_DragStart += On_DragStart;
        EasyTouch.On_DragEnd += On_DragEnd;
    }

    void OnDisable()
    {
        UnsubscribeEvent();
    }

    void OnDestroy()
    {
        UnsubscribeEvent();
    }

    void UnsubscribeEvent()
    {
        EasyTouch.On_Drag -= On_Drag;
        EasyTouch.On_DragStart -= On_DragStart;
        EasyTouch.On_DragEnd -= On_DragEnd;
    }


    void Start()
    {
        dataService = GameObject.FindGameObjectWithTag("DataService").GetComponent<CreateDBSampoerna>();
        masterController = gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<MasterController>();
        //textMesh = (TextMesh)GetComponentInChildren<TextMesh>();
        // startColor = gameObject.GetComponent<Renderer>().material.color;
    }

    // At the drag beginning 
    void On_DragStart(Gesture gesture)
    {

        // Verification that the action on the object
        if (gesture.pickedObject == gameObject)
        {
            fingerIndex = gesture.fingerIndex;
            RandomColor();
            initObjPos = this.transform.position;
            // the world coordinate from touch
            Vector3 position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position);
            deltaPosition = position - transform.position;

            initPosition = Camera.main.WorldToScreenPoint(position).y;
            Debug.Log(initPosition);

        }
    }

    // During the drag
    void On_Drag(Gesture gesture)
    {

        // Verification that the action on the object
        if (gesture.pickedObject == gameObject && fingerIndex == gesture.fingerIndex)
        {

            // the world coordinate from touch
            Vector3 position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position);
            transform.position = position - deltaPosition;
            
          
            // Get the drag angle
            float angle = gesture.GetSwipeOrDragAngle();

           // textMesh.text = angle.ToString("f2") + " / " + gesture.swipe.ToString();
        }
    }

    // At the drag end
    void On_DragEnd(Gesture gesture)
    {

        // Verification that the action on the object
        if (gesture.pickedObject == gameObject)
        {
            Vector3 position = gesture.GetTouchToWorldPoint(gesture.pickedObject.transform.position);
            transform.position = position - deltaPosition;

            endPos = Camera.main.WorldToScreenPoint(position).y;
            Debug.Log(endPos);

            Debug.Log(endPos - initPosition);
            if(endPos - initPosition >  tolerance)
            {
                
                masterController.SentToTarget(redererObj,thisIndex);
                gameObject.transform.position = initObjPos;
            }

            // gameObject.GetComponent<Renderer>().material.color = startColor;
            //textMesh.text = "Drag me";
        }
    }

    private void RandomColor()
    {
        //gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
    }
}
