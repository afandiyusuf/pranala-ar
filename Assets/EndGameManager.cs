﻿using UnityEngine;
using System.Collections;

public class EndGameManager : MonoBehaviour {

	public void GotoIngame()
    {
        Application.LoadLevel("Ingame");
    }

    public void GotoMainMenu()
    {
        Application.LoadLevel("MainMenu");
    }
    public void OpenPranala()
    {
        Application.OpenURL("http://www.pranala.id");
    }
    
}
