﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WarningFormController : MonoBehaviour {

    public Text warning_teks;
    public FormHandler formHandler;
	// Use this for initialization
	void Start () {
        gameObject.SetActive(false);
	}

    public void ShowWarning(string tekswarnign)
    {
        warning_teks.text = tekswarnign;
       
    }

    public void ShowSuccess(string teksSuccess)
    {
        warning_teks.text = teksSuccess;
        gameObject.SetActive(true);
    }

    public void disableWarning()
    {
        gameObject.SetActive(false);
        formHandler.enableSubscribeBtn();
    }
	
}
