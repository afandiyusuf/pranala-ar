﻿using SQLite4Unity3d;

public class UserTable {
    [PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public string nama { get; set; }
    public string no_hp { get; set; }
    public string company { get; set; }
    public string email { get; set; }
    public int is_send { get; set; }
}
