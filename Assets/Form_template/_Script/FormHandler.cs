﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FormHandler : MonoBehaviour {
    // Use this for initialization
    private DataService ds;
    public InputField nama;
    public InputField email;
    public InputField no_hp;
    public InputField company;
    public WarningFormController warningPanel;
    public GameObject subscribeBtn;
    public GameObject subscribeAgain;
    void Start () {
        ds = new DataService("userDatabase.db");
        ds.CreateInitDb();
        gameObject.SetActive(false);
    }

    public void ShowRegister()
    {
        nama.text = "";
        email.text = "";
        no_hp.text = "";
        company.text = "";

        gameObject.SetActive(true);
        //subscribeBtn.SetActive(false);
        //subscribeAgain.SetActive(false);
    }
    public void HideSubscribe()
    {
        gameObject.SetActive(false);
        //subscribeBtn.SetActive(true);
        //subscribeAgain.SetActive(false);
    }
    public void enableSubscribeBtn()
    {
        subscribeAgain.SetActive(true);
    }
    public void InsertSubscribeForm()
    {
        if (nama.text == "" || email.text == "" || no_hp.text == "" || company.text == "")
        {
            warningPanel.ShowWarning("Data harus diisi semua");
        }else{
            ds.InsertToUserTable(nama.text, email.text, no_hp.text, company.text);
            warningPanel.ShowSuccess("Terima kasih");
            gameObject.SetActive(false);

        }
    }
}