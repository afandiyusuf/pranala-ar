﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class CreateDBScript : MonoBehaviour {

	public Text DebugText;

	// Use this for initialization
	void Start () {
		StartSync();
	}

    private void StartSync()
    {
        
        var ds = new DataService("tempDatabase.db");
        ds.CreateInitDb();
        
       
    }
	
	private void ToConsole(IEnumerable<MissionData> missions){
		foreach (var mission in missions) {
			ToConsole(mission.ToString());
		}
	}
	
	private void ToConsole(string msg){
		DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}
}
