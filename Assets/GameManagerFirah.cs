﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerFirah : MonoBehaviour {
    public float CallInterval = 3;
    public float CallIntensity = 1;
    public float ThisScore;
    public float GameTimer = 30;
    public float currentSec;
    //public bool isCalled;
    public GameObject Gentongs;
    public GameObject child;
    public GameObject BefCallParticle;
    public Text scoreText;
    public GameObject Particles;

    private AudioSource mejikSound;

	void Start()
    {
        mejikSound = GameObject.FindGameObjectWithTag("MagicSound").GetComponent<AudioSource>();
        ThisScore = 0;
        AddScore(0);
    }

    void Update()
    {
        GameTimer -= Time.deltaTime;
        currentSec += Time.deltaTime;

        if(currentSec > CallInterval)
        {
            Instantiate(BefCallParticle);
            Invoke("CallAllGentong", 1);
            currentSec = 0;
            mejikSound.Play();

        }
    }

    public void AddScore(int _val)
    {
        ThisScore += _val;
        scoreText.text = ThisScore.ToString();
        PlayerPrefs.SetFloat("score", ThisScore);
    }

    void CallAllGentong()
    {
        for (int i = 0; i < CallIntensity; i++)
        {
            GameObject temp = (GameObject)Instantiate(Gentongs);

            temp.transform.SetParent(gameObject.transform, false);
            Debug.Log(temp.transform.parent);
            Debug.Log(gameObject.transform);
        }
    }
}
