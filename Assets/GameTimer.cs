﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

    float currentTimer = 0;
    public float gameTimer = 60;
    public int currentTik = 0;
    Text timerText;
	// Use this for initialization
	void Start () {
        timerText = GetComponent<Text>();
        PlayerPrefs.SetFloat("score", 0);
	}

    void Update()
    {
        currentTimer += Time.deltaTime;
        currentTik = Mathf.FloorToInt(currentTimer);
       

        if(currentTik > gameTimer)
        {
            float bestScore = PlayerPrefs.GetFloat("bestscore", 0);
            float thisScore = PlayerPrefs.GetFloat("score", 0);
            if(thisScore > bestScore)
            {
                PlayerPrefs.SetFloat("bestscore", thisScore);
            }
            Application.LoadLevel("EndGames");
        }

        if (gameTimer - currentTik < 0)
            timerText.text = "0";
        else
            timerText.text = (gameTimer - currentTik).ToString();
    }
	
}
