﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class CreateDBSampoerna : MonoBehaviour {
    private DataServiceSampoerna ds;
	// Use this for initialization
	void Start () {
		StartSync();
	}

    private void StartSync()
    {
        ds = new DataServiceSampoerna("votingSampoerna.db");
        ds.CreateInitDb();
    }

    public void StartVoting(int voting)
    {
        ds.InsertVoting(voting);
    }

    public void UpdateSenddata(int id)
    {
        ds.UpdateSenddata(id);
        
    }
    public int GetTotalVote(int index)
    {
        var votes = ds.GetVoting(index);
        int i = 0;
        foreach (var vote in votes)
        {
            i++;
        }
        return i;
    }
}
