﻿using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;
using System;
using System.Globalization;

public class DataServiceSampoerna  {

	private  SQLiteConnection _connection;
    private  MissionData[] missionData;
	public DataServiceSampoerna(string DatabaseName){

#if UNITY_EDITOR
            var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID 
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
			var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
			// then save to Application.persistentDataPath
			File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
            _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH: " + dbPath);     

	}
    
    //execute hanya untuk pertama kali
	public void InitDb(){

        CreateInitDb();
    }

	public IEnumerable<MissionData> GetAllMission(){
		return _connection.Table<MissionData>();
	}



	public IEnumerable<MissionData> RequestNewMission(){
		return _connection.Table<MissionData>().Where(x => (x.is_active != false && x.tiers_complete < x.total_tiers)).OrderBy(y => (y.mission_name));
	}

    public IEnumerable<VotingDataTable> RequestUnsendData()
    {
        return _connection.Table<VotingDataTable>().Where(x => (x.is_send != 0));
    }

    public IEnumerable<VotingDataTable> GetVoting(int index)
    {
        return _connection.Table<VotingDataTable>().Where(x => (x.voting == index));
    }

    public IEnumerable<VotingDataTable> UpdateSenddata(int id)
    {
        return _connection.Query<VotingDataTable>("UPDATE VotingDataTable SET is_send=1 WHERE id=1 ");
    }

    public void InsertVoting(int voting, string _nama="default", string _no_hp = "default", string _company = "default", string _email = "default")
    {
        _connection.Insert(new VotingDataTable
        {
            nama = _nama,
            no_hp = _no_hp,
            email = _email,
            company = _company,
            voting = voting,
            is_send = 0
        });
    }
    
	public void CreateInitDb(){

        if (PlayerPrefs.GetString("sampoernaVoting", "not inited") == "not inited")
        {
            _connection.DropTable<VotingDataTable>();
            _connection.CreateTable<VotingDataTable>();
            PlayerPrefs.SetString("sampoernaVoting", "inited");
        }
    }
}
